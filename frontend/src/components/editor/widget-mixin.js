import { mapActions } from 'vuex'
import SignalWatcher from '@/components/SignalWatcher'
import SignalController from '@/components/SignalController'

var widgetMixin = {
  props: {
    mode: { },
    type: { },
    editable: { },
    item: { default: null },
  },

  model: {
    prop: 'item',
  },

  computed: {
    currentItem: function() {
      return this.item;
    }
  },

  components: {
    SignalWatcher,
    SignalController,
  },

  data: function() {
    return {
      currentValue: 0,
      outputValue: 0,
    };
  },

  created() {
    this.listeners = {};
    for(var key in this.$listeners) {
      this.listeners[key] = this.$listeners[key];
    }
    if(this.listeners.input !== undefined) {
      this.listeners.input = this.changed;
    }
    this.componentName = this.$parent.$options._componentTag;
  },

  watch: {
    item: {
      handler: function(newValue, oldValue) {
        //this.$emit('changed', newValue);
        //console.log("Done", newValue);
      }
    },
    deep: true,
  },

  methods: {
    ...mapActions([
      'saveItem',
      'removeItem',
    ]),

    changed: function(event) {
      this.$emit('input', this.item);
    },

    signalChanged: function(value) {
      this.currentValue = value;
    }
  }
};

export default widgetMixin;

