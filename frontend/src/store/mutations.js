import Vue from 'vue'

// Mutation to set resources in state
export const setResources = (state, resources) => {
  state.resources = resources || {}
}

// Mutation too new grid item in resources
export const setNewGridItem = (state, payload) => {
  Vue.set(state.resources, payload.i, payload);
}

// Mutation to remove item from the resources
export const removeItem = (state, payload) => {
  if (payload.i !== undefined) {
    state.resources[payload.i] = undefined;
    delete state.resources[payload.i];
  }
}

// Mutation to update item in resources
export const setUpdatedItem = (state, payload) => {
  if (payload.i !== undefined) {
    state.resources[payload.i] = payload
  }
}

export const setDashboards = (state, payload) => {
  state.dashboards = payload
}

export const setSettings = (state, payload) => {
  state.settings = payload
}

export const setUser = (state, payload) => {
  state.user = payload
}
