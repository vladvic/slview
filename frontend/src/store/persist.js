import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'constructor',
  storage: window.localStorage
});

export default vuexPersist
