// Import helper lib module
import Vue from 'vue'
import axios from 'axios'
import * as lib from './lib'
// Importing sample JSON data for the state
//import data from '../../static/data.json'

function authorizedRequest(state, url, method, data = undefined) {
  return new Promise((resolve, reject) => {
    let token = state.user.token;
    if(token === undefined) {
      reject(undefined);
      return;
    }
    axios({
      url: url,
      method: method,
      data: data,
      headers: {'Authorization': 'Token ' + token}
    })
    .then(data => {
      resolve(data);
    })
    .catch(data => {
      console.log("Error: ", data);
      reject(data);
    });
  });
}

// Action to fetch resources and set in state through the mutation
export const fetchResources = ({commit, state}, id) => {
  //commit('setResources', []); // Initialize resources with empty array
  let db = state.dashboards;
  if(db[id] === undefined) {
    commit('setResources', {});
    commit('setSettings', {});
    return
  }
  let config = JSON.parse(db[id].config);
  let resources = config.resources;
  let settings = config.settings;
  settings.name = db[id].name;
  settings.id = id;
  commit('setResources', resources);
  commit('setSettings', settings);
}

export const selectDashboard = ({commit, state}, id) => {
  let dashboards = state.dashboards;
  let dash = dashboards[id];
  let config = JSON.parse(dash.config);
  let settings = config.settings;
  let resources = {};
  
  config.resources = config.resources.map((widget) => {
    if(widget.component === undefined) {
      if(widget.type === 'switch') {
        widget.component = 'ControlWidget';
        widget.accessType = 'write';
        widget.props = { };
      }
      else {
        widget.component = 'GaugeWidget';
        widget.props = {
          type: widget.type,
        };
        widget.accessType = 'update';
      }
    }
    return widget;
  });

  for(var widgetId in config.resources) {
    var widget = config.resources[widgetId];
    resources[widget.i] = widget;
  }

  settings.name = dash.name;
  settings.id = id;

  commit('setResources', resources);
  commit('setSettings', settings);
}

export const clearDashboard = ({commit, state}, unused) => {
  commit('setResources', {});
  commit('setSettings', {'name': 'New dashboard'});
}

export const actionLogout = ({commit, state}, unused) => {
  commit('setUser', {});
}

export const saveCurrentDashboard = ({commit, state}, url) => {
  let resources = state.resources;
  let settings = state.settings;
  let config = {
    'resources': Object.values(resources),
    'settings':  settings,
  };
  let data = {
    'name':   settings.name,
    'wsurl':  settings.serverUrl,
    'config': JSON.stringify(config),
    'uuid':   lib.guid()
  };
  let id = settings.id || '';

  authorizedRequest(state, url + id, 'put', data)
    .then(data => {
      let dash = data.data;
      let config = JSON.parse(dash.config);
      let id = dash.id;
      let dashboards = state.dashboards;
      let settings = config.settings;

      settings.id = id;
      dashboards[id] = dash;

      commit('setSettings', settings);
      commit('setDashboards', dashboards);
    })
    .catch(data => { });
}

export const deleteDashboard = ({commit, state}, {url, id}) => {
  let dashboards = state.dashboards;
  delete dashboards[id];
  authorizedRequest(state, url + id, 'delete')
    .then(data => {
      commit('setDashboards', dashboards);
    })
    .catch(data => {
    });
}

export const loadDashboards = ({commit, state}, url) => {
  return new Promise((resolve, reject) => {
    authorizedRequest(state, url, 'get')
    .then(data => {
      let db = data.data;
      let dbs = {};
      for(let i = 0; i < db.length; i ++) {
        dbs[db[i].id] = db[i];
      }
      commit('setDashboards', dbs);
      resolve(data.data);
    })
    .catch(data => {
      reject(data);
    });
  });
}

export const actionLogin = ({commit, state}, {url, username, password}) => {
  return new Promise((resolve, reject) => {
    axios
      .post(url, {'username': username, 'password': password})
      .then(data => {
        let token = data.data.token;
        commit('setUser', {token: token});
        console.log("Token: ", token);
        resolve(data);
      })
      .catch(data => {
        console.log("Error: ", data);
        reject(data);
      });
  });
}

// Action to add a widget
export const addItem = ({commit, state}, payload) => {
  let g = lib.guid()
  payload.i = g;
  commit('setNewGridItem', payload)
}

// Action to remove item from the state through the mutation
export const removeItem = ({commit, state}, payload) => {
  commit('removeItem', payload)
}

export const setDashboardId = ({commit, state}, payload) => {
  let settings = state.settings;
  settings.id = payload;
  commit('setSettings', settings);
}

export const saveDashboardName = ({commit, state}, payload) => {
  let settings = state.settings;
  settings.name = payload;
  commit('setSettings', settings);
}

export const saveSettings = ({commit, state}, payload) => {
  commit('setSettings', payload);
}

export const saveServerUrl = ({commit, state}, payload) => {
  let settings = state.settings;
  settings.serverUrl = payload;
  commit('setSettings', settings);
}

// Action to save item through the mutation
export const saveItem = ({commit, state}, payload) => {
  commit('setUpdatedItem', payload)
}

