// Getter to retrieve state data
export const getResources = (state) => {
  return state.resources
}

// Getter to retrieve state data
export const getSettings = (state) => {
  return state.settings
}

// Getter to retrieve state data
export const getUser = (state) => {
  return state.user
}

// Getter to retrieve state data
export const getDashboards = (state) => {
  return state.dashboards
}

// Getter to retrieve state data
export const getExportJson = (state) => {
  let json = {
               settings: state.settings,
               resources: state.resources
             };
  return json;
}
