import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Dashboard from '@/components/Dashboard'
import DashboardList from '@/components/DashboardList'
import Auth from '@/components/Auth'
import Constructor from '@/components/Constructor'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/dashboard-list'
    },
    {
      path: '/login',
      name: 'Authorize',
      component: Auth
    },
    {
      path: '/constructor/:itemIndex?',
      name: 'Constructor',
      component: Constructor,
      props: true
    },
    {
      path: '/dashboard/:id?',
      name: 'Dashboard',
      component: Dashboard,
      props: true
    },
    {
      path: '/dashboard-list',
      name: 'DashboardList',
      component: DashboardList
    },
    {
      path: '/edit/:id?',
      name: 'Editor',
      component: Home,
      props: true
    }
  ],
  mode: 'history'
})
