class Signal {
  constructor(name, client, json) {
    this._client = client;
    this.name = name;
    this.path = null;
    this.value = {
      "type": "NONE",
      "value": null
    };
    this.writable = true;
    this.state = {
      "readError": false,
      "writeError": false
    };
    this._watchlist = {
      'write':  [],
      'update': [],
      'state': [],
      'writeError': [],
    };
    if(json) {
      this.parse(json);
    }
  }

  update(val) {
    let msg = {
      "command": "update",
      "signal": {
        "name": this.name
      },
      "value": val.type + ":" + val.value
    };
    this._client.send(JSON.stringify(msg));
  }

  write(val) {
    let msg = {
      "command": "write",
      "signal": {
        "name": this.name
      },
      "value": val.type + ":" + val.value
    };
    this._client.send(JSON.stringify(msg));
  }

  unwatch(type, fn) {
    if(this._watchlist[type] === undefined) {
      return;
    }
    for(let i = 0; i < this._watchlist[type].length; i ++) {
      if(this._watchlist[type][i] == fn) {
        this._watchlist[type].splice(i, 1);
        i --;
      }
    }
  }

  watch(type, fn) {
    let subType = null;
    let writeSubscribed = this._watchlist['write'].length + this._watchlist['writeError'].length;
    let updateSubscribed = this._watchlist['update'].length + this._watchlist['state'].length;
    if(type == 'write' || type == 'writeError') {
      if(writeSubscribed == 0) {
        subType = 'SUB_WRITE';
      }
      if(updateSubscribed == 0) {
        let msg = {
          "signal": {
            "name": this.name
          },
          "command": "read"
        };
        this._client.send(JSON.stringify(msg));
      }
    }
    else {
      if(updateSubscribed == 0) {
        subType = 'SUB_UPDATE';
      }
    }
    this._watchlist[type].push(fn);
    if(subType !== null) {
      let msg = {
        "signal": {
          "name": this.name
        },
        "command": "subscribe",
        "type": subType
      };
      this._client.send(JSON.stringify(msg));
    }
  }

  _resubscribe() {
    let writeSubscribed = this._watchlist['write'].length + this._watchlist['writeError'].length;
    let updateSubscribed = this._watchlist['update'].length + this._watchlist['state'].length;
    if(writeSubscribed) {
      let msg = {
        "signal": {
          "name": this.name
        },
        "command": "subscribe",
        "type": 'SUB_WRITE'
      };
      this._client.send(JSON.stringify(msg));
    }
    if(updateSubscribed) {
      let msg = {
        "signal": {
          "name": this.name
        },
        "command": "subscribe",
        "type": 'SUB_UPDATE'
      };
      this._client.send(JSON.stringify(msg));
    }
  }

  parse(json) {
    let cmd = json['event'];
    let value = null;

    if(cmd == 'update' || cmd == 'write') {
      let avalue = json['value'].split(':');
      value = {
        'type': avalue[0],
        'value': avalue[1]
      }
    }
    let state = json['state'];
    let signal = json['signal'];

    if(cmd == 'update') {
      if(value !== null) {
        this._watchlist['update'].forEach(
            (fn) => {
              fn(this, value);
            }
          )
        this.value = value;
      }
      else if(state !== null) {
        this._watchlist['state'].forEach(
            (fn) => {
              fn(this, state);
            }
          )
        this.state = state;
      }
    }
    else if(cmd == 'write') {
      this._watchlist['write'].forEach(
          (fn) => {
            fn(this, value);
          }
        )
    }
    else if(cmd == 'writeError') {
      this._watchlist['writeError'].forEach(
          (fn) => {
            fn(this);
          }
        )
    }
  }
}

class Client {
  constructor(wsurl) {
    this.connected = false;
    this.signals = {};
    this.messages = [];
  }

  disconnect() {
    if(this.socket !== undefined) {
      this.connected = false;
      this.socket.close();
    }
  }

  connect(wsurl) {
    this.wsurl = wsurl;
    this.socket = new WebSocket(this.wsurl);
    this.socket.onopen = (e) => {
      this.open(e);
    };
    this.socket.onmessage = (e) => {
      this.message(e);
    };
    this.socket.onclose = (e) => {
      this.close(e);
    };
    this.socket.onerror = (e) => {
      this.error(e);
    };
  }

  send(msg) {
    try {
      if(this.connected) {
        this.socket.send(msg);
      }
      else {
        this.messages.push(msg);
      }
    }
    catch(e) {
      console.log("Error sending message...");
    }
  }

  signal(name) {
    let signal = this.signals[name] || new Signal(name, this);
    this.signals[name] = signal;
    return signal;
  }

  signals() {
    let result = [];
    for(signal in this.signals) {
      result.push(signal);
    }
    return result;
  }

  fetchAllSignals(onNewSignal) {
    let msg = {
      "signal": {
        "mask": ""
      },
      "command": "read"
    };
    this.onNewSignal = onNewSignal;
    this.send(JSON.stringify(msg));
  }

  open(event) {
    this.socket = event.target;
    this.connected = true;
    if(this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
    this.messages.forEach((msg) => {
      this.socket.send(msg);
    })
    this.messages = [];
    Object.values(this.signals).forEach((signal) => {
      signal._resubscribe();
    })
  }

  message(event) {
    let data = JSON.parse(event.data);
    name = data.signal.name;
    if(this.signals[name]) {
        this.signals[name].parse(data);
    }
    else {
      this.signals[name] = new Signal(name, this, data);
      if(this.onNewSignal) {
        this.onNewSignal(this.signals[name]);
      }
    }
  }

  close(event) {
    console.log("Connection closed");
    this.connected = false;
    if(this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
    if(!event.wasClean) {
      this.connect(this.wsurl);
    }
  }

  error(event) {
    console.log("Failed to connect");
    this.connected = false;
    this.timeout = setTimeout(5000, () => this.connect(this.wsurl));
  }
}

let client = new Client();

export default client;

