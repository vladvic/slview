from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from . import models
from . import serializers
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated

class DashboardView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        """
        Return a list of all dashboards.
        With dashboard_id return a dashboard by id or null.
        """

        dashboard_id = kwargs.get("dashboard_id", None)
        user = request.user

        result = None
        return_status = status.HTTP_404_NOT_FOUND

        if dashboard_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if dashboard_id is None:
                owned = [dash for dash in models.Dashboard.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Dashboard.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Dashboard.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = serializers.DashboardSerializer()
                result = [serializers.DashboardSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Dashboard.objects.filter(pk = dashboard_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or 
                    dash.filter(can_view = user).exists() or 
                    dash.filter(owner_id = user).exists()):
                    result = serializers.DashboardSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user).exists())
                    return_status = status.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        """
        Delete dashboard
        """
        dashboard_id = kwargs.get("dashboard_id", None)
        user = request.user

        return_status = status.HTTP_404_NOT_FOUND
        result = None

        if dashboard_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Dashboard.objects.filter(pk = dashboard_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or 
                dash.filter(can_view = user).exists() or 
                dash.filter(owner_id = user).exists()):
                result = serializers.DashboardSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)


    def put(self, request, format=None, **kwargs):
        """
        Modify or create dashboard
        """
        dashboard_id = kwargs.get("dashboard_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if dashboard_id is not None:
                dash = models.Dashboard.objects.filter(pk = dashboard_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or 
                    dash.filter(owner_id = user).exists()):
                    dash = models.Dashboard.objects.filter(pk = dashboard_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = serializers.DashboardSerializer(dash[0], data=data)
                else:
                    print("Dashboard doesn't belong to the user");
                    return_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Dashboard.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = serializers.DashboardSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {
                        "errors": serializer.errors
                    }
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

