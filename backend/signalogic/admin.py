from django.contrib import admin
from .models import Dashboard, DashboardEditor, DashboardViewer

admin.site.register(Dashboard)
admin.site.register(DashboardEditor)
admin.site.register(DashboardViewer)
