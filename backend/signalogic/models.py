from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class Dashboard(models.Model):
    def __str__(self):
        return 'Dashboard: ' + self.name
    owner_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    can_edit = models.ManyToManyField(get_user_model(), through="DashboardEditor", related_name='editable')
    can_view = models.ManyToManyField(get_user_model(), through="DashboardViewer", related_name='viewable')
    uuid  = models.CharField(max_length=48, blank=True, default='')
    wsurl = models.CharField(max_length=48, blank=True, default='')
    name  = models.CharField(max_length=30, blank=True, default='')
    config = models.TextField()

class DashboardEditor(models.Model):
    dashboard = models.ForeignKey(Dashboard, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

class DashboardViewer(models.Model):
    dashboard = models.ForeignKey(Dashboard, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
