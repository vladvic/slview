from rest_framework import serializers
from . import models

class DashboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dashboard
        fields = '__all__'

class DashboardViewer(serializers.ModelSerializer):
    class Meta:
        model = models.DashboardViewer
        fields = '__all__'

class DashboardEditorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DashboardEditor
        fields = '__all__'
