from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from . import models
from . import serializers
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from django.contrib.auth import authenticate, logout

class AuthView(APIView):
    parser_classes = [JSONParser]

    def get(self, request, format=None, **kwargs):
        operation = kwargs.get("operation", None)
        if operation == "logout":
            request.user.auth_token.delete()
            logout(request)
        else:
            return JsonResponse(None, status=status.HTTP_400_BAD_REQUEST, safe=False)

        return JsonResponse(None, status=status.HTTP_200_OK, safe=False)

