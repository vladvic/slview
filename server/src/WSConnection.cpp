/***************************************************
 * WSConnection.cpp
 * Created on Tue, 09 Jul 2019 09:41:38 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <iostream>
#include "WSConnection.h"

WSMessage::WSMessage(int type, const char *message, size_t msglen)
  : type(type)
  , data(message, message + msglen)
{ }

WSMessage::WSMessage()
  : type(LWS_WRITE_TEXT)
{ }

WSMessage::WSMessage(int type, std::vector<char> &&msg)
  : type(type)
  , data(std::move(msg))
{ }

WSConnection::WSConnection(struct lws *socket)
  : mSocket(socket)
  , mUserData(NULL)
{
}

WSMessage *WSConnection::read() {
  if(mCurrent.data.size() > 0) {
    return &mCurrent;
  }
  return NULL;
}

int WSConnection::dataAvailable(int type, char *in, size_t size) {
  mCurrent.data.insert(mCurrent.data.end(), in, in + size);
  mCurrent.type = type;
  return 0;
}

void WSConnection::resetMessage() {
  mCurrent.data.resize(0);
}

int WSConnection::lwsCallback(enum lws_callback_reasons reason, void *in, size_t len) {
  switch(reason) {
  case LWS_CALLBACK_SERVER_WRITEABLE:
    if(mMessages.empty()) {
      return 0;
    }
    std::lock_guard<std::mutex> lock(mLock);
    WSMessage &msg = mMessages.front();
    lws_write(mSocket, (unsigned char *)msg.data.data() + LWS_PRE, msg.data.size() - LWS_PRE, (lws_write_protocol)msg.type);
    mMessages.pop();
    if(!mMessages.empty()) {
      lws_callback_on_writable(mSocket);
    }
    return 0;
  }
  return -1;
}

int WSConnection::write(int type, const char *message, size_t msglen) {
  std::vector<char> out(LWS_PRE);
  out.insert(out.end(), message, message + msglen);

  {
  std::lock_guard<std::mutex> lock(mLock);
  mMessages.emplace(type, std::move(out));
  }

  lws_context *ctx = lws_get_context(mSocket);

  lws_callback_on_writable(mSocket);
  if(ctx) {
    lws_cancel_service(ctx);
  }

  return msglen;
}
