/***************************************************
 * WSServer.cpp
 * Created on Tue, 09 Jul 2019 07:24:51 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <iostream>
#include <string>
#include <stdexcept>
#include <unistd.h>
#include "WSServer.h"
#include "WSConnection.h"

struct lws_protocols WSServer::sProtocols[] = {
	{ "http", lws_callback_http_dummy, 0, 0 },
	{ \
		"signal-router", \
		&WSServer::lwsCallback, \
		sizeof(WSConnection), \
		128, \
		0, NULL, 1024 \
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

static lws_protocol_vhost_options defproto = {
  NULL, NULL, "default", "1"
};

static lws_protocol_vhost_options pvo = {
	NULL,		/* "next" pvo linked-list */
	&defproto,	/* "child" pvo linked-list */
	"signal-router",	/* protocol name we belong to on this vhost */
	""		/* ignored */
};

WSServer::WSServer(const char *address, int port, std::unique_ptr<IWSHandler> &&handler)
  : mHandler(std::move(handler))
{
  if(sProtocols[1].user != NULL) {
    throw std::runtime_error("WSServer instance already created!");
  }
  sProtocols[1].user = this;

	int logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE;

	lws_set_log_level(logs, NULL);
	struct lws_context_creation_info info;
	memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
	info.port = port;
	info.mounts = NULL;
  info.pvo = &pvo;
	info.protocols = sProtocols;
  info.options = LWS_SERVER_OPTION_SKIP_SERVER_CANONICAL_NAME;

	mContext = lws_create_context(&info);
	if (!mContext) {
		throw std::runtime_error("LWS init failed");
	}
}

WSServer::~WSServer() {
	lws_context_destroy(mContext);
}

int WSServer::lwsCallback(struct lws *wsi, 
                          enum lws_callback_reasons reason,
                          void *user, void *in, size_t len) {
  const lws_protocols *protocol = lws_get_protocol(wsi);
  if(protocol) {
    WSServer *srv = (WSServer*)(protocol->user);
    return srv->callback(wsi, reason, user, in, len);
  }
  return 0;
}

int WSServer::callback(struct lws *wsi, 
                       enum lws_callback_reasons reason, 
                       void *user, void *in, size_t len) {
  WSConnection *conn = (WSConnection *)user;

  switch (reason) {
  case LWS_CALLBACK_WSI_DESTROY:
    if(conn) {
      conn->~WSConnection();
    }
    return 0;
  case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
    /* add ourselves to the list of live pss held in the vhd */
    {
    new (conn) WSConnection(wsi);
    return onConnect(conn);
    }
    break;

  case LWS_CALLBACK_ESTABLISHED:
    if(conn) {
      return onConnectionAdded(conn);
    }

  case LWS_CALLBACK_CLOSED:
    /* remove our closing pss from the list of live pss */
    if(conn) {
      onConnectionRemoved(conn);
    }
    break;

  case LWS_CALLBACK_SERVER_WRITEABLE:
    if(conn) {
      return conn->lwsCallback(reason, in, len);
    }
    break;

  case LWS_CALLBACK_RECEIVE:
    {
    int type = !lws_frame_is_binary(wsi) ? LWS_WRITE_TEXT : LWS_WRITE_BINARY;
    if(lws_is_first_fragment(wsi)) {
      conn->resetMessage();
    }
    conn->dataAvailable(type, (char*)in, len);
    if(lws_is_final_fragment(wsi)) {
      onDataAvailable(conn);
    }
    }
    break;

  case LWS_CALLBACK_EVENT_WAIT_CANCELLED:
    lws_callback_on_writable_all_protocol(mContext, &sProtocols[1]);
    break;

  default:
    break;
  }

  return 0;
}

void WSServer::process(int microseconds) {
  if (lws_service(mContext, microseconds)) {
    throw std::runtime_error("Interrupted");
  }
}

int WSPrintHandler::onConnect(WSConnection *conn) {
  std::cout << "Connecting: " << conn << std::endl;
  return 0;
}

int WSPrintHandler::onConnectionAdded(WSConnection *conn) {
  std::cout << "Connection added: " << conn << std::endl;
  return 0;
}

void WSPrintHandler::onConnectionRemoved(WSConnection *conn) {
  std::cout << "Connection removed: " << conn << std::endl;
}

int WSPrintHandler::onDataAvailable(WSConnection *conn) {
  WSMessage *msg = conn->read();
  if(msg) {
    std::cout << "On data: " << std::string(msg->data.data(), msg->data.size()) << std::endl;
    conn->write(msg->type, msg->data.data(), msg->data.size());
  }
  return 0;
}

