/***************************************************
 * WSServer.h
 * Created on Tue, 09 Jul 2019 07:23:51 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <memory>
#include <libwebsockets.h>
#include "WSConnection.h"

namespace util {

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

}

class IWSHandler {
public:
  virtual ~IWSHandler() {}
  virtual int  onConnect(WSConnection *conn) = 0;
  virtual int  onConnectionAdded(WSConnection *conn) = 0;
  virtual void onConnectionRemoved(WSConnection *conn) = 0;
  virtual int  onDataAvailable(WSConnection *conn) = 0;
};

class WSPrintHandler : public IWSHandler {
public:
  virtual int  onConnect(WSConnection *conn);
  virtual int  onConnectionAdded(WSConnection *conn);
  virtual void onConnectionRemoved(WSConnection *conn);
  virtual int  onDataAvailable(WSConnection *conn);
};

class WSServer {
public:
  WSServer(const char *address, int port, std::unique_ptr<IWSHandler> &&handler = std::move(util::make_unique<WSPrintHandler>()));
  ~WSServer();
  void process(int microseconds);

private:
  int  onConnect(WSConnection *conn) { return mHandler ? mHandler->onConnect(conn) : 0; }
  int  onConnectionAdded(WSConnection *conn) { return mHandler ? mHandler->onConnectionAdded(conn) : 0; }
  void onConnectionRemoved(WSConnection *conn) { if(mHandler) mHandler->onConnectionRemoved(conn); }
  int  onDataAvailable(WSConnection *conn) { return mHandler ? mHandler->onDataAvailable(conn) : 0; }
  lws_context *mContext;
  std::unique_ptr<IWSHandler> mHandler;
  static
  int lwsCallback(struct lws *wsi, enum lws_callback_reasons reason,
                  void *user, void *in, size_t len);
  int callback(struct lws *wsi, enum lws_callback_reasons reason,
               void *user, void *in, size_t len);
  static struct lws_protocols sProtocols[];
};

