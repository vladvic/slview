/***************************************************
 * boardclient.cpp
 * Created on Sun, 09 Jun 2019 11:08:58 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <iostream>
#include <algorithm>
#include <cctype>
#include <stdexcept>
#include <sstream>
#include <signal.h>
#include "WSClient.h"

WSClient::WSClient() {
  init();
}

WSClient::~WSClient() {
  destroy();
}

int WSClient::process(int ms) {
  postProcess();
  return Client::process(ms);
}

void WSClient::onConnected() {
}

void WSClient::onWriteSignalValue(Signal *s, const SignalValue &v) {
  auto &subs = s->userData<SignalSubs>()->subs[IDX_WRITE];

  for(auto connection = subs.begin(); connection != subs.end(); ++ connection) {
    sendWriteValue(s, v, *connection);
  }
}

void WSClient::onWriteSignalError(Signal *s) {
  auto &subs = s->userData<SignalSubs>()->subs[IDX_UPDATE];

  for(auto connection = subs.begin(); connection != subs.end(); ++ connection) {
    std::stringstream ss;
    ss << "{" << eventJson("writeError") << ",";
    ss << "\"signal\":" << signalToJson(s) << "}";
    std::string msg(ss.str());
    (*connection)->write(LWS_WRITE_TEXT, msg.data(), msg.size());
  }
}

int  WSClient::onConnect(WSConnection *conn) {
  return 0;
}

int  WSClient::onConnectionAdded(WSConnection *conn) {
  conn->setUserData(new ConnectionState);
  return 0;
}

void WSClient::onConnectionRemoved(WSConnection *conn) {
  ConnectionState *state = conn->userData<ConnectionState>();
  if(state) {
    for(int i = 0; i < 2; i ++) {
      std::set<std::string> &subs = state->subs[i];
      for(auto sub = subs.begin(); sub != subs.end(); ++ sub) {
        Signal *s = findSignal(*sub);
        if(s) {
          SignalSubs *sstate = s->userData<SignalSubs>();
          sstate->subs[i].erase(conn);
          if(sstate->subs[i].empty()) {
            unsubscribe(i == IDX_UPDATE ? SUB_UPDATE : SUB_WRITE, SS_NAME, s->name());
          }
        }
      }
    }
    delete state;
  }
}

int  WSClient::onDataAvailable(WSConnection *conn) {
  WSMessage *message = conn->read();
  if(!message) {
    return 0;
  }

  std::string msg(message->data.data(), message->data.size());
  using namespace pjson;
  document doc;
  doc.deserialize_in_place((char*)msg.c_str());

  try {
    if(!doc.has_key("command")) {
      throw std::runtime_error("Invalid JSON: no command specified");
    }

    std::string command = doc["command"].as_string();
    std::transform(command.begin(), command.end(), command.begin(),
                  [](unsigned char c){ return std::tolower(c); });
    SignalSpec spec = getSignalSpec(doc);
    int stype = std::get<0>(spec);
    std::string sid = std::get<1>(spec);

    if(command == "read") {
      read(stype, sid.c_str());
      auto signals = findSignals(spec);
      for(auto s = signals.begin(); s != signals.end(); ++ s) {
        onUpdateSignalValue(*s, (*s)->value());
      }
    }
    else if(command == "update") {
      SignalValue v = getValue(doc);
      postUpdate(stype, sid.c_str(), SST_VALUE, ST_VALUE, (signal_value_s*)v);

      /*
      auto signals = findSignals(spec);
      for(auto s = signals.begin(); s != signals.end(); ++ s) {
        if(*s) {
          (*s)->update(v);
        }
        onUpdateSignalValue(*s, v);
      }
      */
    }
    else if(command == "write") {
      SignalValue v = getValue(doc);
      postWrite(stype, sid.c_str(), ST_VALUE, (signal_value_s*)v);

      /*
      auto signals = findSignals(spec);
      for(auto s = signals.begin(); s != signals.end(); ++ s) {
        if(*s) {
          (*s)->write(v);
        }
        onWriteSignalValue(*s, v);
      }
      */
    }
    else if(command == "subscribe") {
      int type = getType(doc);
      read(stype, sid.c_str());
      auto signals = findSignals(spec);
      for(auto s = signals.begin(); s != signals.end(); ++ s) {
        auto &updates = (*s)->userData<SignalSubs>()->subs[IDX_UPDATE];
        auto &writes = (*s)->userData<SignalSubs>()->subs[IDX_UPDATE];

        if(updates.find(conn) == updates.end() &&
           writes.find(conn) == writes.end()) {
          sendUpdateValue(*s, (*s)->value(), conn);
        }

        addSubscription(*s, type, conn);
      }
    }
    else if(command == "unsubscribe") {
      int type = getType(doc);
      auto signals = findSignals(spec);
      for(auto signal = signals.begin(); signal != signals.end(); ++ signal) {
        Signal *s = *signal;
        if(!s) {
          return 0;
        }

        removeSubscription(*signal, type, conn);
      }
    }
    else {
      throw std::runtime_error("Invalid command");
    }
  }
  catch(const std::exception &e) {
    sendError(conn, e.what());
  }

  postProcess();

  return 0;
}

std::list<Signal*> WSClient::findSignals(const SignalSpec &spec) {
  int type = std::get<0>(spec);
  std::string id = std::get<1>(spec);

  switch(type) {
  case SS_NAME:
    {
    Signal *s = findSignal(id);
    if(s) {
      return { s };
    }
    }
    break;
  case SS_TYPE:
    return findSignalsByType(id);
  case SS_MASK:
    return findSignalsByMask(id);
  }
  return std::list<Signal*>();
}

void WSClient::addSubscription(Signal *s, int type, WSConnection *conn) {
  if(type > 1 || type < 0) {
    return; // wrong type
  }

  auto &subs = s->userData<SignalSubs>()->subs[type];

  // Check if this connection is already subscribed
  if(subs.find(conn) == subs.end()) {
    auto &csubs = conn->userData<ConnectionState>()->subs[type];
    csubs.insert(s->name());
    if(subs.empty()) {
      s->subscribe(type == IDX_UPDATE ? SUB_UPDATE : SUB_WRITE);
    }
    subs.insert(conn);
  }
}

void WSClient::removeSubscription(Signal *s, int type, WSConnection *conn) {
  if(type > 1 || type < 0) {
    return; // wrong type
  }

  auto &subs = s->userData<SignalSubs>()->subs[type];

  // Check if this connection is already subscribed
  if(subs.find(conn) != subs.end()) {
    auto &csubs = conn->userData<ConnectionState>()->subs[type];
    csubs.erase(s->name());
    subs.erase(conn);
    if(subs.empty()) {
      s->unsubscribe(type == IDX_UPDATE ? SUB_UPDATE : SUB_WRITE);
    }
  }
}

WSClient::SignalSpec WSClient::getSignalSpec(const pjson::value_variant &value) {
  if(!value.has_key("signal")) {
    throw std::runtime_error("Invalid JSON: no signal specified");
  }
  auto &doc = value["signal"];
  if(!doc.is_object()) {
    throw std::runtime_error("Invalid JSON: signal specification is invalid");
  }
  if(doc.has_key("name")) {
    return std::make_tuple(SS_NAME, doc["name"].as_string());
  }
  if(doc.has_key("mask")) {
    return std::make_tuple(SS_MASK, doc["mask"].as_string());
  }
  if(doc.has_key("type")) {
    return std::make_tuple(SS_TYPE, doc["type"].as_string());
  }
  throw std::runtime_error("Invalid JSON: signal specification is invalid");
}

SignalValue WSClient::getValue(const pjson::value_variant &doc) {
  SignalValue result;
  if(!doc.has_key("value")) {
    throw std::runtime_error("Invalid JSON: no value specified");
  }
  result.fromPrintable(doc["value"].as_string());
  return result;
}

int WSClient::getType(const pjson::value_variant &doc) {
  if(!doc.has_key("type")) {
    throw std::runtime_error("Invalid JSON: no subscription type specified");
  }
  std::string type = doc["type"].as_string();
  if(type == "SUB_WRITE") {
    return SUB_WRITE;
  }
  if(type == "SUB_UPDATE") {
    return SUB_UPDATE;
  }
  throw std::runtime_error("Invalid subscription type");
}

void WSClient::sendWriteValue(Signal *s, const SignalValue &v, WSConnection *conn) {
  std::stringstream ss;
  ss << "{" << eventJson("write") << ",";
  ss << "\"signal\":" << signalToJson(s) << ",";
  ss << "\"value\":" << valueToJson(v) << "}";
  std::string msg(ss.str());
  conn->write(LWS_WRITE_TEXT, msg.data(), msg.size());
}

void WSClient::sendUpdateValue(Signal *s, const SignalValue &v, WSConnection *conn) {
  std::stringstream ss;
  ss << "{" << eventJson("update") << ",";
  ss << "\"signal\":" << signalToJson(s) << ",";
  ss << "\"value\":" << valueToJson(v) << "}";
  std::string msg(ss.str());
  conn->write(LWS_WRITE_TEXT, msg.data(), msg.size());
}

void WSClient::onUpdateSignalValue(Signal *s, const SignalValue &v) {
  auto &subs = s->userData<SignalSubs>()->subs[IDX_UPDATE];

  for(auto connection = subs.begin(); connection != subs.end(); ++ connection) {
    sendUpdateValue(s, v, *connection);
  }
}

void WSClient::onUpdateSignalState(Signal *s, const SignalState &v) {
  auto &subs = s->userData<SignalSubs>()->subs[IDX_UPDATE];

  for(auto connection = subs.begin(); connection != subs.end(); ++ connection) {
    std::stringstream ss;
    ss << "{" << eventJson("update") << ",";
    ss << "\"signal\":" << signalToJson(s) << ",";
    ss << "\"state\":" << stateToJson(v) << "}";
    std::string msg(ss.str());
    (*connection)->write(LWS_WRITE_TEXT, msg.data(), msg.size());
  }
}

std::string WSClient::eventJson(const std::string &event) {
  std::string cmd = "\"event\":\"";
  cmd += event;
  cmd += "\"";
  return cmd;
}

std::string WSClient::commandJson(const std::string &command) {
  std::string cmd = "\"command\":\"";
  cmd += command;
  cmd += "\"";
  return cmd;
}

std::string WSClient::signalToJson(const Signal *s) {
  std::stringstream ss;
  ss << "{\"name\":\"" << s->name() << "\",";
  ss <<  "\"id\":"   << s->id() << ",";
  ss <<  "\"path\":\""   << s->path() << "\",";
  ss <<  "\"state\":"  << stateToJson(s->state()) << ",";
  ss <<  "\"writable\":"  << s->isWritable() << ",";
  ss <<  "\"value\":"  << valueToJson(s->value()) << "}";
  return ss.str();
}

std::string WSClient::valueToJson(const SignalValue &v) {
  std::string value = "\"" + v.printable() + "\"";
  return value;
}

std::string WSClient::stateToJson(const SignalState &s) {
  std::string state = std::string("{\"readError\":") + (s.readFailure() ? "true" : "false") + ",\"writeError\":" + 
                                 (s.writeFailure() ? "true" : "false") + "}";
  return state;
}

void WSClient::sendError(WSConnection *conn, const char *msg) {
  std::string message = std::string("{\"event\":\"error\",\"message\":\"") + msg + "\"}";
  conn->write(LWS_WRITE_TEXT, message.data(), message.size());
}

void WSClient::onAddSignal(Signal *s) {
  s->setUserData(new SignalSubs);
  std::cout << "Adding signal: " << s->name() << ": " << s->value().printable() << std::endl;
}

void WSClient::onDeleteSignal(Signal *s) {
  delete s->userData<SignalSubs>();
  std::cout << "Deleting signal: " << s->name() << ": " << s->value().printable() << std::endl;
}

