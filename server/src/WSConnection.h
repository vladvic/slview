/***************************************************
 * WSConnection.h
 * Created on Tue, 09 Jul 2019 09:10:06 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <mutex>
#include <queue>
#include <vector>
#include <libwebsockets.h>

class WSServer;

struct WSMessage {
  WSMessage(int type, const char *message, size_t msglen);
  WSMessage(int type, std::vector<char> &&msg);
  WSMessage();
  int type;
  std::vector<char> data;
};

class WSConnection {
public:
  WSConnection(struct lws *socket);
  int lwsCallback(enum lws_callback_reasons reason, void *in, size_t len);
  int write(int type, const char *message, size_t msglen);
  WSMessage *read();

  template<typename T>
  T* userData() {
    return (T*)mUserData;
  }
  template<typename T>
  void setUserData(T *data) {
    mUserData = (void*)data;
  }

private:
  friend class WSServer;
  int dataAvailable(int type, char *in, size_t size);
  void resetMessage();

  WSMessage mCurrent;
  std::mutex mLock;
  std::queue<WSMessage> mMessages;
  struct lws *mSocket;
  void *mUserData;
};

