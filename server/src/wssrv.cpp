/***************************************************
 * boardmain.cpp
 * Created on Sun, 09 Jun 2019 11:13:19 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <signal.h>
#include <string>
#include <iostream>
#include "WSClient.h"

#define RECONNECTS  10

void output_help(const char *what) {
  printf("Usage: %s [options]\n", what);
  printf("Options:\n");
  printf("--port,-p <port>            server port, default 8888\n");
  printf("--address,-a <address>      server address, default localhost\n");
  printf("--ws-port,-w <port>         websocket port, default 8084\n");
  printf("--ws-address,-r <address>   websocket address, default localhost\n");
  printf("--help,-h                   output help\n");
}

int parseCommandLine(int argc, char **argv, int &port, std::string &address, int &wsport, std::string &wsaddress) {
  static struct option long_options[] =
  {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
    {"port",    1, 0, 'p'},
    {"address", 1, 0, 'a'},
    {"ws-port",    1, 0, 'w'},
    {"ws-address", 1, 0, 'r'},
    {"help",    0, 0, 'h'},
    {0, 0, 0, 0}
  };
  int option_index = 0;

  optind = 1;

  while(1) {
    int c = getopt_long(argc, argv, "hp:a:w:r:", long_options, &option_index);

    if(c == -1)
      break;

    switch(c) {
    case 'p':
      port = atoi(optarg);
      break;
    case 'a':
      address = optarg;
      break;
    case 'w':
      wsport = atoi(optarg);
      break;
    case 'r':
      wsaddress = optarg;
      break;
    case 'h':
      output_help(argv[0]);
      return -1;
    case '?':
      output_help(argv[0]);
      return -1;
    }
  }

  return 0;
}

bool running = true;

void stop(int s) {
  running = 0;
}

int main(int argc, char **argv) {
  int port = 8888;
  std::string address = "127.0.0.1";
  int wsport = 8084;
  std::string wsaddress = "127.0.0.1";

  struct sigaction a;
  a.sa_handler = &stop;
  a.sa_flags = 0;
  sigemptyset(&a.sa_mask);
  sigaction(SIGINT, &a, NULL);

  int result = parseCommandLine(argc, argv, port, address, wsport, wsaddress);

  if(result != 0) {
    return result;
  }

  try {
    WSClient *client = new WSClient;
    WSServer server(wsaddress.c_str(), wsport, std::unique_ptr<IWSHandler>(client));

    client->setRunning(true);
    client->connect(address.c_str(), port);

    while(running && client->isRunning()) {
      if(client->process(100) < 0) {
        for(int i = 0; i < RECONNECTS && running; i ++) {

          try {
            client->connect(address.c_str(), port);
            break;
          }
          catch(std::exception &e) {
            if(i > (RECONNECTS - 2)) {
              std::cout << "Reconnect failed" << std::endl;
              return -1;
            }
            usleep(1000000);
          }
        }
      }
      else {
        server.process(1);
      }
    }
    client->setRunning(false);
  }
  catch(std::exception &e) {
    std::cout << argv[0] << ": Error: " << e.what() << "\n" << std::endl;
    output_help(argv[0]);
  }

  return 0;
}

