/***************************************************
 * boardclient.h
 * Created on Sun, 09 Jun 2019 11:11:14 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <string>
#include <vector>
#include <tuple>
#include <list>
#include <queue>
#include <array>
#include <memory>
#include <map>
#include <set>
#include <pjson.h>
#include <cpp/Client.hpp>
#include "WSConnection.h"
#include "WSServer.h"

#define IDX_UPDATE 0
#define IDX_WRITE  1

struct SignalSubs {
  std::set<WSConnection*> subs[2];
};

struct ConnectionState {
  std::set<std::string> subs[2];
};

class WSClient
  : public Client
  , public IWSHandler
{
public:
  WSClient();
  virtual ~WSClient();
  int process(int ms);

  // Client
  virtual void onConnected();
  virtual void onWriteSignalValue(Signal *s, const SignalValue &v);
  virtual void onWriteSignalError(Signal *s);
  virtual void onUpdateSignalValue(Signal *s, const SignalValue &v);
  virtual void onUpdateSignalState(Signal *s, const SignalState &v);
  virtual void onAddSignal(Signal *s);
  virtual void onDeleteSignal(Signal *s);

  // IWSHandler
  virtual int  onConnect(WSConnection *conn);
  virtual int  onConnectionAdded(WSConnection *conn);
  virtual void onConnectionRemoved(WSConnection *conn);
  virtual int  onDataAvailable(WSConnection *conn);

private:
  typedef std::tuple<int, std::string> SignalSpec;
  std::list<Signal*> findSignals(const SignalSpec &);

  /* Create json */
  std::string commandJson(const std::string &command);
  std::string eventJson(const std::string &command);
  std::string signalToJson(const Signal *s);
  std::string stateToJson(const SignalState &s);
  std::string valueToJson(const SignalValue &v);
  void sendError(WSConnection *conn, const char *msg);
  void sendUpdateValue(Signal *s, const SignalValue &v, WSConnection *conn);
  void sendWriteValue(Signal *s, const SignalValue &v, WSConnection *conn);

  /* Parse json */
  SignalSpec getSignalSpec(const pjson::value_variant &doc);
  SignalValue getValue(const pjson::value_variant &doc);
  int getType(const pjson::value_variant &doc);

  void addSubscription(Signal *s, int type, WSConnection *conn);
  void removeSubscription(Signal *s, int type, WSConnection *conn);
};

