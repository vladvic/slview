#!/bin/sh

objdump=$1
bin=$2
dir=$2
sysroot=$3

echo "Objdump: $objdump"
echo "Binary folder: $bin"
echo "Sysroot: $sysroot"

if test -z "$bin" -o -z "$sysroot" -o -z "$objdump"
then
  echo "Usage: copydlls.sh <objdump> <binary folder> <sysroot>"
  exit
fi

for exe in `ls $bin/*.exe 2>/dev/null`;
do
  echo Processing dependencies for: $exe
  dlls="${dlls} "`$objdump --private-headers $exe | grep \\\\.dll | sed -e's/^.*: //'`
  dlls="${dlls} "`$objdump --private-headers $exe | grep \\\\.DLL | sed -e's/^.*: //'`
done

echo $dlls | tr " " "\n" | sort | uniq | xargs -t -i find $sysroot -name "{}" -print -quit 2>/dev/null | xargs -i cp "{}" $dir/
old=0
new=`ls -a $bin/*.dll | wc -l`

while test $old -ne $new;
do
  dlls=''

  for exe in `ls $bin/*.dll 2>/dev/null`;
  do
    dlls="${dlls} "`$objdump --private-headers $exe | grep \\\\.dll | sed -e's/^.*: //'`
    dlls="${dlls} "`$objdump --private-headers $exe | grep \\\\.DLL | sed -e's/^.*: //'`
  done

  echo $dlls | tr " " "\n" | sort | uniq | xargs -t -i find $sysroot -name "{}" -print -quit 2>/dev/null | xargs -i cp "{}" $dir/
  old=$new
  new=`ls -a $bin/*.dll | wc -l`
done

#x86_64-w64-mingw32-objdump --private-headers $exe | grep \\.dll | sed -e's/^.*: /\/usr\/x86_64-w64-mingw32\/sys-root\/mingw\/bin\//' | \
#xargs ls -1 2>/dev/null | xargs -i echo "DLL: {}"
#x86_64-w64-mingw32-objdump --private-headers $exe | grep \\.dll | sed -e's/^.*: /\/usr\/x86_64-w64-mingw32\/sys-root\/mingw\/bin\//' | \
#xargs ls -1 2>/dev/null | xargs -i cp -n "{}" $dir/

